# Maintainer: Fabian Bornschein <fabiscafe-cat-mailbox-dog-org>
# Contributor: Jan Alexander Steffens (heftig) <heftig@archlinux.org>
# Contributor: Jan de Groot <jgc@archlinux.org>

pkgbase=nautilus
pkgname=(nautilus libnautilus-extension)
pkgver=43.beta1
pkgrel=3
pkgdesc="Default file manager for GNOME"
url="https://wiki.gnome.org/Apps/Files"
arch=(x86_64)
license=(GPL)
depends=(libgexiv2 gnome-desktop-4 gvfs dconf tracker3 tracker3-miners
         gnome-autoar gst-plugins-base-libs libadwaita libportal-gtk4)
makedepends=(gobject-introspection git gtk-doc meson libcloudproviders appstream-glib)
checkdepends=(python-gobject)
options=(debug)
_commit=3b4a1a31bd00c91184e1dc7196fd0d53a8d58022  # tags/43.beta.1^0
source=("git+https://gitlab.gnome.org/GNOME/nautilus.git#commit=$_commit"
        "git+https://gitlab.gnome.org/GNOME/libgd.git")
sha256sums=('SKIP'
            'SKIP')

pkgver() {
  cd $pkgbase
  git describe --tags | sed -r 's/([a-z])\./\1/;s/[^-]*-g/r&/;s/-/+/g'
}

prepare() {
  cd $pkgbase

  git submodule init
  git submodule set-url subprojects/libgd "$srcdir/libgd"
  git submodule update
}

build() {
  arch-meson $pkgbase build \
    -D docs=true
  meson compile -C build
}

check() {
  meson test -C build --print-errorlogs
}

_pick() {
  local p="$1" f d; shift
  for f; do
    d="$srcdir/$p/${f#$pkgdir/}"
    mkdir -p "$(dirname "$d")"
    mv "$f" "$d"
    rmdir -p --ignore-fail-on-non-empty "$(dirname "$f")"
  done
}

package_nautilus() {
  depends+=(libnautilus-extension libcloudproviders)
  optdepends=('nautilus-sendto: Send files via mail extension')
  provides=("nautilus43=$pkgver")
  conflicts=(nautilus43)
  replaces=(nautilus43)
  groups=(gnome)

  meson install -C build --destdir "$pkgdir"

  cd "$pkgdir"

  _pick libne usr/include
  _pick libne usr/lib/{girepository-1.0,libnautilus-extension*,pkgconfig}
  _pick libne usr/share/{gir-1.0,gtk-doc}
}

package_libnautilus-extension() {
  pkgdesc="Library for extending the $pkgdesc"
  depends=(gtk4)
  provides=(libnautilus43-extension-2 libnautilus-extension-2 libnautilus-extension.so)
  conflicts=(libnautilus43-extension-2 libnautilus-extension-2)
  replaces=(libnautilus43-extension-2 libnautilus-extension-2)
  mv libne/* "$pkgdir"
}

# vim:set sw=2 et:
