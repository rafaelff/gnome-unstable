# Maintainer: Fabian Bornschein <fabiscafe-cat-mailbox-dog-org>
# Contributor: Jan Alexander Steffens (heftig) <heftig@archlinux.org>

pkgname=tracker3-miners
pkgver=3.4.0beta
pkgrel=1
pkgdesc="Collection of data extractors for Tracker/Nepomuk"
url="https://wiki.gnome.org/Projects/Tracker"
arch=(x86_64)
license=(GPL)
depends=(tracker3 gst-plugins-base-libs exempi libexif libcue libgrss libgsf
         libgxps libiptcdata libosinfo poppler-glib totem-plparser giflib
         libgexiv2 gupnp-dlna upower libseccomp libnm)
makedepends=(git meson vala asciidoc)
checkdepends=(python-gobject python-tappy gst-plugins-good gst-plugins-base
              gst-libav)
groups=(gnome)
options=(debug)
_commit=ef5117845460f658c09b3328fbbdad489f0498ee  # tags/3.4.0.beta^0
source=("git+https://gitlab.gnome.org/GNOME/tracker-miners.git#commit=$_commit")
sha256sums=('SKIP')

pkgver() {
  cd tracker-miners
  git describe --tags | sed -r 's/\.([a-z])/\1/;s/[^-]*-g/r&/;s/-/+/g'
}

prepare() {
  cd tracker-miners
}

build() {
  arch-meson tracker-miners build \
    -D tests_tap_protocol=true
  meson compile -C build
}

check() {
  # Intermittent test failures: https://gitlab.gnome.org/GNOME/tracker-miners/-/issues/79
  dbus-run-session meson test -C build --print-errorlogs -t 3 || :
}

package() {
  depends+=(libtracker-sparql-3.0.so)

  meson install -C build --destdir "$pkgdir"
}

# vim:set sw=2 et:

